package dataDriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class RegisterApachi {

	public static void main(String[] args) throws IOException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		File f = new File("/home/global/Documents/Test_data/Testdata2.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook book = new XSSFWorkbook(fis);
		XSSFSheet Sheet =book.getSheetAt(1);
		int rows= Sheet.getPhysicalNumberOfRows();
		for(int i=1;i<rows;i++) {
			String firstname = Sheet.getRow(i).getCell(0).getStringCellValue();
			String lastname = Sheet.getRow(i).getCell(1).getStringCellValue();
		String email =Sheet.getRow(i).getCell(2).getStringCellValue();
		String pw= Sheet.getRow(i).getCell(3).getStringCellValue();
		String confrmpw = Sheet.getRow(i).getCell(4).getStringCellValue();
		
		driver.findElement(By.linkText("Register")).click();
		driver.findElement(By.id("gender-female")).click();
		driver.findElement(By.id("FirstName")).sendKeys(firstname);
		driver.findElement(By.id("LastName")).sendKeys(lastname);
		driver.findElement(By.id("Email")).sendKeys(email);
		driver.findElement(By.id("Password")).sendKeys(pw);
		driver.findElement(By.id("ConfirmPassword")).sendKeys(confrmpw);
		driver.findElement(By.id("register-button")).click();
		WebElement txt =driver.findElement(By.xpath("//li[contains(text(),'The specified email already exists')]"));
		txt.click();
		String t= txt.getText();
		System.out.println(t);
		driver.quit();
		
		
		

	}

}}
