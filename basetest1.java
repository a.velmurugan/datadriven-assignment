package dataDriventesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class basetest1 {

@Test(dataProvider = "logindata", dataProviderClass = dataprovider.class)
    
    public void TestLogin(String username, String password) { 
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        
        driver.get("https://demowebshop.tricentis.com/");
        
        WebElement login = driver.findElement(By.linkText("Log in"));
         login.click();
        driver.findElement(By.id("Email")).sendKeys(username);
        driver.findElement(By.id("Password")).sendKeys(password);
        driver.findElement(By.xpath("//input[@value='Log in']")).click();
        driver.findElement(By.linkText("Log out")).click();
        driver.close();
    }


	}


